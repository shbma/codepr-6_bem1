var http = require("http"),
    fs = require("fs"),
    url = require("url"),
    path = require("path");

http.createServer(function(req,res){
    var mimeTypes = {
        '.js' : 'text/javascript',
        '.css': 'text/css',
        '.html': 'text/html',
    }

    var pathname = url.parse(req.url).path;
    if(pathname == '/'){
        pathname = '/index.html';
    }
    var extname = path.extname(pathname);

    var mimeType = mimeTypes[path.extname(pathname)];
    pathname = pathname.substring(1,pathname.length);

    fs.readFile(pathname, 'utf8',function(err,data){
        if(err){
            console.log('Could not find of open file for reading...\n')
        } else {
            res.writeHead(200,{'Content-Type':mimeTypes[path.extname(pathname)]});
            res.end(data);
        }
    })
}).listen(8080);

console.log('Server is running...');
